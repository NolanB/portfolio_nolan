<!-- # Welcome to MkDocs

For full documentation visit [mkdocs.org](https://www.mkdocs.org).

## Commands

* `mkdocs new [dir-name]` - Create a new project.
* `mkdocs serve` - Start the live-reloading docs server.
* `mkdocs build` - Build the documentation site.
* `mkdocs -h` - Print help message and exit.

## Project layout

    mkdocs.yml    # The configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files. -->

# **Bienvenue**
Ici vous trouverez mes réalisations et mon curriculum vitae.

## **Cursus**
J'ai suivie une formation de développeur en Intelligence Artificielle (RNCP 34757) - Niv 6 (Bac +3/4) à Simplon Grenoble en partenariat avec Microsoft. Cette formation était une formation en alternance qui avait pour but de nous spécialiser sur le développement de projets IA et des outils Azure.

Vous pouvez retrouver mon profil sur [Linkedin](https://www.linkedin.com/in/nolan-boukachab-406522163/){:target="_blank"} et mes projets sur [GitHub](https://github.com/NolanBoukachab){:target="_blank"} et [GitLab](https://gitlab.com/NolanB){:target="_blank"}.

`Python` `SQL` `data visualisation` `base de données` `scraping` `méthodes agiles` `IA`
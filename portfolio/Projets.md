# <b>Projets</b>

### **DOC-UFCN application** - 2023
![Texte alternatif](src/logo_teklia.png){ align=left; width="32%";}

Une application qui utilise les modèles DOC-UFCN de Téklia sur huggingface pour prédire la position des lignes sur des images manuscrites.
Cette application à été réalisé au sein de mon alternance chez Téklia.

[Lien application](https://huggingface.co/spaces/Teklia/doc-ufcn){:target="_blank"}
 
[Lien huggingface](https://huggingface.co/spaces/Teklia/doc-ufcn/tree/main){:target="_blank"}

Les objectifs étaient :

- Créer une application avec Gradio 
- Créer une documentation pour permettre aux utilisateurs de créer leurs propre application avec une configuration personnalisable
- Contribuer à un projet open source

Compétences : développement d'application

`Gradio` `HuggingFace` `Python`

---

### **NutrIA** - 2022
![Texte alternatif](src/NutrIA.png){ align=left; width="32%";}

Une page web qui demande de renseigner les données d'un produit à un utilisateur dans le but de prédire un Nutriscore.

[Lien application](https://nutria.herokuapp.com/){:target="_blank"}

[Lien GitLab](https://gitlab.com/simplon-dev-ia/grenoble-2021-2022/projets/projet-1/projet-1-groupe-7){:target="_blank"}

Les objectifs étaient :

- récupérer les données de la plateform OpenFoodFacts
- Analyser le jeu de donnéées
- Nettoyer le jeu de données
- Garder les données les plus pertinentes
- Entrainer des modèles IA (KNN et RandomForest)
- Créer un formulaire sur une page Web pour prédire un Nutriscore
- Déployer le site Web sur Heroku

Compétences : data visualisation, conception de base de données, développement web

`HTML` `Bulma` `Python` `FastAPI` `Scikit-Learn` `Heroku`

---

### **Stage Data : Chambéry Savoie Experts** - 2020 juin et juillet
![Texte alternatif](src/Dossiers_en_cours_page.png){ align=left; width="32%";}
![Texte alternatif](src/Coût_moyen_page.png){ align=left; width="32%";} 
![Texte alternatif](src/Map_page.png){ align=left; width="32%";} 

Un dashboard qui afficherait les statistiques de chacun des experts par compte utilisateur et les statistiques de chaque réparateur sur une carte géographique.

**Le projet est un projet privé, je n'ai pas eu la permission de diffuser le lien vers le GitLab.**

Les objectifs étaient :

- comprendre le besoin client
- établir un cahier des charges
- concevoir une base de données
- créer des comptes utilisateurs
- récuperer les addresses postales des réparateurs via une API
- insérer les données dans une base
- afficher les données de la base par utilisateur sur une page web
- analyser les données et les classer par catégories

Compétences : conception de CDC, data visualisation, conception de base de données, développement web

`HTML` `Bootstrap` `Python` `Django` `Postgresql` `API`

---

### **Projet Watt'Sim** - 2020
![Texte alternatif](src/Wattsim.png){ align=left; width="32%";}

Une page web affichant les donées de maquettes éducatives dans le but de sensibiliser les jeunes aux énergies vertes.

**Le projet est un projet privé, je n'ai pas eu la permission de diffuser le lien vers le GitLab.**

Les objectifs étaient :

- récupéer les données via un simulateur
- créer des comptes utilisateurs professeurs/élèves
- concevoir une base de données
- insérer les données dans une base
- afficher les données de la base par utilisateur sur une page web

Compétences : data visualisation, conception de base de données, développement web

`HTML` `Bootstrap` `Python` `Django` `Postgresql`

---

### **Agrégateur de news** - 2020
![Texte alternatif](src/Agrégateur.png){ align=left; width="32%";}

Une page web permettant de lire des news venant de différents fluxs RSS.

[Lien application](https://news-aggregator-env-staging.herokuapp.com/login){:target="_blank"}

[Lien GitLab](https://gitlab.com/JulienAtSimplon/p09_aggregateurnews){:target="_blank"}

Les objectifs étaient :

- intégrer des fluxs type RSS
- que chaque compte utilisateur ait ses propres flux

Compétences : développement web

`HTML` `Bootstrap` `Python` `Flask` `Postgresql`

---

### **Patrimoine de Grenoble** - 2020

![Texte alternatif](src/Patrimoine.png){ align=left; width="32%";}

Une page Jupyter Notebook qui affiche une carte avec les lieux historiques de Grenoble classés par thématique.

[Lien application](https://nolanb.gitlab.io/patrimoine_grenoble/Patrimoine_Grenoble.html){:target="_blank"}

[Lien GitLab](https://gitlab.com/JulienAtSimplon/p09_aggregateurnews){:target="_blank"}

Les objectifs étaient :

- récuperer les données via un fichier Json
- nettoyer les données
- insérer les données dans une base
- afficher les données de la base par catégorie sur la carte

Compétences : data visualisation, nettoyage des données

`Python` `Sqlite` `Folium`

---

### **Mighty Beards** - Projet étudiant fin d'année 2018
<iframe lass="d-block w-100" width="560" height="315" src="https://www.youtube.com/embed/jXF-uoGcHgE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
Un jeu coopératif à 2 joueurs (1 clavier et l'autre manette).

Les objectifs étaient de faire une IA:

- qui suivrait l'un des joueurs lorsqu'ils entreraient dans son champ de vision
- qui auraient plusieurs patterns d'attaque
- qui partirait à la recherche de son casque lorsque l'un des joueurs lui aurait ôté

Compétences : IA

`Blueprint` `UE4` `Blackboard`